import torch
from torch import nn, optim
from torch.utils.data import DataLoader

from Net import Net
from trash.Loader import Loader


# do_EM()

def train_agedb(min_mae=25.5, only_test=False):
    loader = Loader()
    print("Loading images...")
    loader.load()

    if torch.cuda.is_available():
        device = torch.device('cuda:0')
        print('Using GPU')
    else:
        device = torch.device('cpu')
        print('Using CPU')

    batch_size = 128
    dataloader_test = DataLoader(loader.test, batch_size=batch_size, shuffle=True)
    mae = nn.L1Loss()

    if not only_test:
        model = Net().to(device)
        optimizer = optim.Adam(model.parameters(), lr=0.00001, weight_decay=0.00001)

        # create batches
        dataloader_train = DataLoader(loader.training, batch_size=batch_size, shuffle=True)
        dataloader_valid = DataLoader(loader.validation, batch_size=batch_size, shuffle=True)
        best_validation_mae = 1000
        best_train_mae = 1000

        for epoch in range(1, 70):
            loss_function = nn.NLLLoss()
            train_acc, valid_acc, validation_mae, train_mae = [], [], [], []
            model.train()

            for data, target in dataloader_train:
                data = data.to(device)
                target = target.to(device)
                data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(True)
                target = target.clone().detach().requires_grad_(True)
                # print(batch)
                distribution = model(data).view(data.size()[0], 100)

                # prediction
                batch_prediction = torch.argmax(distribution, dim=1)

                # loss
                optimizer.zero_grad()
                loss = loss_function(distribution.log(), target.long() - 1)

                train_acc.append(torch.mean((batch_prediction == target).float()).item())
                loss.backward()
                optimizer.step()
                train_mae.append(mae(batch_prediction, target).item())
            train_mae_mean = (torch.mean(torch.Tensor(train_mae)).float()).item()
            if best_train_mae > train_mae_mean:
                best_train_mae = train_mae_mean

            print("Training epoch: ", epoch, " Training MAE: ", best_train_mae)

            # evaluation part
            model.eval()

            for data, target in dataloader_valid:
                data = data.to(device)
                target = target.to(device)
                data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(False)
                target = target.clone().detach().requires_grad_(False)
                distribution = model(data).view(data.size()[0], 100)

                # argmin_by_y (sum_by_y'[p(y'|x)*|y' - y|]
                age_diffs = abs(torch.arange(1, 101).reshape(-1, 1) - torch.arange(1, 101)).repeat(data.size()[0],
                                                                                                   1).to(
                    device)
                batch_prediction = (distribution.repeat(1, 100).view(data.size()[0] * 100, 100) * age_diffs).sum(
                    dim=1).view(data.size()[0], 100).argmin(dim=1)
                valid_acc.append(torch.mean((batch_prediction == target).float()).item())
                validation_mae.append(mae(batch_prediction, target).item())

            validation_mae_mean = (torch.mean(torch.Tensor(validation_mae)).float()).item()
            if validation_mae_mean <= min_mae:
                torch.save(model, 'data/initial_model_slightly_learned_on_criminals.pt')
                print("MAE of saved: " + str(validation_mae_mean))
                print(validation_mae_mean)
                break
            if best_validation_mae > validation_mae_mean:
                best_validation_mae = validation_mae_mean
                torch.save(model, 'initial_model_slightly_learned_on_criminals.pt')

            print("Validation epoch: ", epoch, " MAE: ", validation_mae_mean, "Best MAE: ", best_validation_mae)

    # test
    model = torch.load('data/initial_model_slightly_learned_on_criminals.pt')
    model.eval()
    best_test_mae = 100
    test_mae = []
    for data, target in dataloader_test:
        data = data.to(device)
        target = target.to(device)
        data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(False)
        target = target.clone().detach().requires_grad_(False)
        distribution = model(data).view(data.size()[0], 100)

        # argmin_by_y (sum_by_y'[p(y'|x)*|y' - y|]
        age_diffs = abs(torch.arange(1, 101).reshape(-1, 1) - torch.arange(1, 101)).repeat(data.size()[0], 1).to(device)
        batch_prediction = (distribution.repeat(1, 100).view(data.size()[0] * 100, 100) * age_diffs).sum(
            dim=1).view(data.size()[0], 100).argmin(dim=1)

        test_mae.append(mae(batch_prediction, target).item())
    test_mae_mean = (torch.mean(torch.Tensor(test_mae)).float()).item()

    if best_test_mae > test_mae_mean:
        best_test_mae = test_mae_mean

    print(" Best MAE: ", best_test_mae)
