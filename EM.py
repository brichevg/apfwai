import math
import os
import shutil

from torch import optim, nn

from Expectation import Expectation
from Net import Net
import matplotlib

import torch as torch
from torch.nn import functional as f
import matplotlib.pyplot as plt


class EM:
    def __init__(self, loader):

        # training data
        train_bag_limit = round(loader.bag_size.size()[0] * 0.7)
        validation_bag_limit = loader.bag_size.size()[0]

        self.bag_sizes_train = loader.bag_size[0:train_bag_limit].squeeze()
        self.imgs_train = loader.bags_of_images[0:sum(self.bag_sizes_train).int()]
        self.bags_creation_year_train = loader.bags_creation_year[0:sum(self.bag_sizes_train).int()]

        # validation data
        self.bag_sizes_validation = loader.bag_size[train_bag_limit:validation_bag_limit].squeeze()
        self.imgs_validation = loader.bags_of_images[
                               sum(self.bag_sizes_train).int():sum(self.bag_sizes_train).int() + sum(
                                   self.bag_sizes_validation.int())]
        self.bags_creation_year_validation = loader.bags_creation_year[
                                             sum(self.bag_sizes_train).int():sum(self.bag_sizes_train).int() + sum(
                                                 self.bag_sizes_validation.int())]

        # test data
        # self.imgs_test = loader.bags_of_images[500:sum(self.bag_sizes_train.int()[500:550])]

        self.device = loader.device

        # train features
        self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train \
            = self.init_all_priors(loader.bag_size, loader.bags_creation_year)

        # self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train \
        #     = self.init_priors(self.bag_sizes_train, self.bags_creation_year_train)
        #
        # # validation features
        # self.birth_ranges_validation, self.feasible_birth_years_validation, self.birth_priors_validation \
        #     = self.init_priors(self.bag_sizes_validation, self.bags_creation_year_validation)

        self.e_layer = Expectation(self.device).to(
            self.device)

        self.bags_real_birth_year = loader.bags_real_birth_year

    def prediction_for_bag(self, alphas_for_bag):
        alphas_repeated = torch.t(alphas_for_bag.repeat(self.feasible_birth_years_train.size()[0], 1))
        fb_for_min_repeated = self.feasible_birth_years_train.repeat(alphas_for_bag.size()[0], 1)
        fb_for_sum_repeated = torch.t(self.feasible_birth_years_train.repeat(alphas_for_bag.size()[0], 1))
        sec = torch.abs(fb_for_min_repeated - fb_for_sum_repeated)
        sums = torch.sum(alphas_repeated * sec, 0)
        return self.feasible_birth_years_train[torch.argmin(sums)]

    def plot_alphas(self, bag_sizes, alphas, epoch, loss):
        f = plt.figure(0)
        alphas_formatted = alphas.view(bag_sizes.size()[0], self.feasible_birth_years_train.size()[0])
        bag_i = 0
        for bag_i_x in range(0, 5):
            for bag_i_y in range(0, 4):
                if bag_i == len(bag_sizes):
                    break
                plt.subplot2grid((5, 4), (bag_i_x, bag_i_y))
                # plt.subplot2grid((math.ceil(len(bag_sizes) / 2), math.floor(len(bag_sizes) / 2)), (bag_i_x, bag_i_y))
                plt.plot(self.feasible_birth_years_train.cpu().clone().numpy(),
                         alphas_formatted[bag_i].detach().cpu().clone().numpy())
                plt.axvline(x=self.bags_real_birth_year[bag_i], color='red')
                bag_i = bag_i + 1
        if loss != -1:
            f.suptitle('Epoch #{}, Loss: {}'.format(epoch, loss), fontsize=20)
        plt.show()
        plt.draw()
        f.savefig('posterior/img_{}.png'.format(epoch))
        plt.clf()

    def e(self, model, data, bag_sizes, birth_priors, birth_ranges, bags_creation_year, feasible_birth_years):
        birth_d = torch.Tensor([]).to(self.device)
        age_d = torch.Tensor([]).to(self.device)
        offset = 0
        mae = nn.L1Loss()
        prediction = []
        target = []
        mae_v = []
        for bag_i in range(0, len(bag_sizes.int())):
            imgs = data[offset:offset + bag_sizes.int()[bag_i]].view(-1, 1, 64, 64)
            print('Allocated memory after bag '+str(bag_i)+': '+str(torch.cuda.memory_allocated(device='cuda')))

            age_distr = model(imgs).view(bag_sizes.int()[bag_i], 100)
            target.append(self.bags_real_birth_year[bag_i].item())

            alphas_for_bag = self.e_layer.forward(
                age_distr, bag_i, birth_priors, birth_ranges[bag_i],
                bags_creation_year[offset:offset + bag_sizes.int()[bag_i]], bag_sizes, birth_ranges,
                feasible_birth_years).to(self.device)

            birth_d = torch.cat((birth_d, alphas_for_bag))

            prediction_bag = self.prediction_for_bag(alphas_for_bag)
            prediction.append(prediction_bag.item())
            mae_v.append(mae(prediction_bag, self.bags_real_birth_year[bag_i]).item())

            age_d = torch.cat((age_d, age_distr))
            offset = offset + bag_sizes.int()[bag_i]
        return age_d, birth_d, prediction, target, mae_v

    def m(self, age_d, birth_d, bag_sizes, creation_years, birth_ranges, optimizer, train=True):
        offset = torch.Tensor([0])
        loss = 0
        for bag_i in range(0, len(bag_sizes.int())):
            bag_i_start = offset.int()
            bag_i_end = (offset + bag_sizes[bag_i]).int()
            bag_size = bag_sizes[bag_i].int().item()
            bag_creations = creation_years[bag_i_start:bag_i_end]
            birth_d_bag = birth_d[bag_i * self.feasible_birth_years_train.size()[0]
                                  :bag_i * self.feasible_birth_years_train.size()[0] +
                                   self.feasible_birth_years_train.size()[0]]

            # distribution indices
            ages = torch.arange(0, 100).repeat(bag_size, 1).long().to(self.device)
            img_idx = torch.t(torch.arange(0, bag_size).repeat(ages.size()[1], 1)).long()
            bag_age_d = age_d[img_idx, ages]

            # betas
            diff_idx = torch.t(bag_creations.repeat(ages.size()[1], 1)) - ages
            alpha_idx_feasible = ((diff_idx >= birth_ranges[bag_i][0]) & (diff_idx <= birth_ranges[bag_i][1]))
            betas = torch.Tensor([self.feasible_birth_years_train[0]]).repeat(diff_idx.size()[0],
                                                                              diff_idx.size()[1]).to(self.device)
            betas[alpha_idx_feasible] = diff_idx[alpha_idx_feasible]
            betas = betas - self.feasible_birth_years_train[0]
            betas[betas != 0] = birth_d_bag[betas[betas != 0].long()]
            loss = loss + torch.sum(betas * torch.log(bag_age_d))

            offset = offset + bag_sizes[bag_i]

        birth_priors = self.calculate_priors(birth_d, bag_sizes).clone()
        # print(birth_priors)
        # print(' ')
        # print(age_d)
        # (' ')

        loss = -loss
        if train:
            loss.backward()
            optimizer.step()
        return loss.item(), birth_priors

    def refresh(self, optimizer, bag_sizes, data, bags_creation_year, birth_ranges, feasible_birth_years, birth_priors):
        optimizer.zero_grad()
        return optimizer, bag_sizes.clone().detach(), data.clone().detach(), bags_creation_year.clone().detach(), \
               birth_ranges.clone().detach(), feasible_birth_years.clone().detach(), birth_priors.clone().detach()

    def calculate_priors(self, birth_d, bag_sizes):

        bag_sizes_repeated = bag_sizes.repeat(self.feasible_birth_years_train.size()[0], 1)
        birth_d_repeated = torch.t(birth_d.view(bag_sizes.size()[0], self.feasible_birth_years_train.size()[0]))
        return torch.sum(bag_sizes_repeated * birth_d_repeated, 1) / sum(
            torch.sum(bag_sizes_repeated * birth_d_repeated, 1))

    def em(self, epochs):
        model = Net().to(self.device)
        optimizer = optim.Adam(model.parameters(), lr=0.01)
        best_validation_loss = float('inf')
        best_model = 0
        best_validation_mae = float('inf')
        best_train_mae = float('inf')
        try:
            shutil.rmtree('posterior')
            os.mkdir('posterior')
        except FileNotFoundError:
            os.mkdir('posterior')

        # self.evaluate_e()

        # train/validation
        # validation_loss, prediction_v, target_v = self.predict(model, optimizer)
        # validation_mae_mean = f.l1_loss(torch.Tensor(prediction_v).squeeze(),
        #                                 torch.Tensor(target_v).squeeze()).float().item()
        # if best_validation_mae > validation_mae_mean:
        #     best_validation_mae = validation_mae_mean
        # print(
        #     'Epoch 0, Validation loss: ' + str(validation_loss) + ', Validation mae: ' + str(
        #         validation_mae_mean) + ', Best validation mae:' + str(best_validation_mae))
        for i in range(1, epochs + 1):
            model.train()
            optimizer, self.bag_sizes_train, self.imgs_train, self.bags_creation_year_train, \
            self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train = \
                self.refresh(optimizer, self.bag_sizes_train, self.imgs_train, self.bags_creation_year_train,
                             self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train)

            age_d, birth_d, prediction, target, mae_v = self.e(model,
                                                               self.imgs_train,
                                                               self.bag_sizes_train,
                                                               self.birth_priors_train,
                                                               self.birth_ranges_train,
                                                               self.bags_creation_year_train,
                                                               self.feasible_birth_years_train)

            if i == 1 and self.device != 'cuda':
                    self.plot_alphas(self.bag_sizes_train, birth_d, i, -1)

            loss, birth_priors = self.m(age_d, birth_d, self.bag_sizes_train,
                                        self.bags_creation_year_train, self.birth_ranges_train, optimizer)

            if self.device != 'cuda' and i!=1:
                self.plot_alphas(self.bag_sizes_train, birth_d, i, loss)

            # print('target: ', target)
            # print('predic: ', prediction)

            train_mae_mean = (torch.mean(torch.Tensor(mae_v)).float()).item()
            self.birth_priors_train = birth_priors.clone()
            if best_train_mae > train_mae_mean:
                best_train_mae = train_mae_mean

            # validation_loss, prediction_v, target_v = self.predict(model, optimizer)
            # validation_mae_mean = f.l1_loss(torch.Tensor(prediction_v).squeeze(), torch.Tensor(target_v).squeeze()).float().item()
            # if best_validation_mae > validation_mae_mean:
            #     best_validation_mae = validation_mae_mean

            print(
                'Epoch ' + str(i) + ', Train loss: ' + str(loss) + ', Train mae:' + str(
                    train_mae_mean) + ', Best train mae:' + str(best_train_mae))

            # print('Epoch ' + str(i) + ', Train loss: ' + str(loss) )

            # if best_validation_loss > validation_loss:
            #     torch.save(model, 'best_model.pt')
            #     best_validation_loss = validation_loss

    def predict(self, model, optimizer):
        with torch.no_grad():
            age_d, birth_d, prediction, target, mae_v = self.e(model,
                                                               self.imgs_validation,
                                                               self.bag_sizes_validation,
                                                               self.birth_priors_validation,
                                                               self.birth_ranges_validation,
                                                               self.bags_creation_year_validation,
                                                               self.feasible_birth_years_validation
                                                               )

            loss, birth_priors = self.m(age_d, birth_d, self.bag_sizes_validation,
                                        self.bags_creation_year_validation, self.birth_ranges_validation, optimizer)

        return loss, prediction, target

    def init_all_priors(self, bag_sizes, bag_creation_years):
        # min and max for each bag
        birth_ranges = torch.empty(bag_sizes.size()[0], 2).to(self.device)
        offset = 0
        b_s = bag_sizes.int()
        for bag_i in range(0, len(b_s)):
            min_cr = min(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            max_cr = max(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            period = 99 - (max_cr - min_cr)
            birth_ranges[bag_i] = torch.Tensor([min_cr - period, min_cr])
            offset = offset + b_s[bag_i]
        bounds = torch.Tensor([torch.min(torch.t(birth_ranges)[0]), torch.max(torch.t(birth_ranges)[1])]).to(
            self.device)
        feasible_byears = torch.arange(bounds[0].item(), bounds[1].item() + 1).to(self.device)
        birth_priors = torch.Tensor([100 / feasible_byears.size()[0] / 100]).repeat(feasible_byears.size()[0]).to(
            self.device)
        return birth_ranges, feasible_byears, birth_priors

    def init_priors(self, bag_sizes, bag_creation_years):
        # min and max for each bag
        birth_ranges = torch.empty(bag_sizes.size()[0], 2).to(self.device)
        offset = 0
        b_s = bag_sizes.int()
        for bag_i in range(0, len(b_s)):
            min_cr = min(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            max_cr = max(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            birth_ranges[bag_i] = torch.Tensor([max_cr - 99, min_cr])
            offset = offset + b_s[bag_i]
        bounds = torch.Tensor([torch.min(torch.t(birth_ranges)[0]), torch.max(torch.t(birth_ranges)[1])]).to(
            self.device)
        feasible_byears = torch.arange(bounds[0].item(), bounds[1].item() + 1).to(self.device)
        birth_priors = torch.Tensor([100 / feasible_byears.size()[0] / 100]).repeat(feasible_byears.size()[0]).to(
            self.device)
        return birth_ranges, feasible_byears, birth_priors

    def evaluate_e(self):
        model = torch.load('initial_model_learned_on_criminals.pt', map_location=torch.device('cpu'))
        model.eval()
        age_distr, alphas, p, t, mae_v = self.e(model,
                                         self.imgs_train,
                                         self.bag_sizes_train,
                                         self.birth_priors_train,
                                         self.birth_ranges_train,
                                         self.bags_creation_year_train,
                                         self.feasible_birth_years_train)
        try:
            os.mkdir('plots')
        except FileExistsError:
            shutil.rmtree('plots')
            os.mkdir('plots')

        for bag_i in range(0, len(self.bag_sizes_train) - 10):
            plt.axvline(x=self.bags_real_birth_year[bag_i], color='red')
            plt.plot(self.feasible_birth_years_train.cpu().clone().numpy(),
                     alphas[int(bag_i * self.feasible_birth_years_train.size()[0]):int(
                         int(bag_i * self.feasible_birth_years_train.size()[0])
                         + self.feasible_birth_years_train.size()[0])].detach().cpu().clone().numpy())
            # plt.show()
            plt.savefig('plots/bag_' + str(bag_i))
            plt.clf()

    #     loss = self.do_m(alphas, age_distr)
    #     print(epochs)
    #     print(loss)
    #     epochs = epochs + 1
    # for bag in bags_to_display:
    #     plt.plot(self.birth_range_across_bags.detach().cpu().clone().numpy(),
    #              alphas[bag].detach().cpu().clone().numpy())
    #     plt.axvline(x=self.bags_real_birth_year[bag], color='red')
    #     plt.savefig('plots_last_epoch/bag_' + str(bag))
    #     plt.clf()
