import copy
import math

import numpy as np
from torch import optim
from torch.nn import functional as f
from Expectation import Expectation
from Net import Net
import matplotlib

matplotlib.use('Agg')
import torch as torch


class EM_cuda:
    def __init__(self, loader):

        # training data
        self.train_offset = 0
        self.validation_offset = math.ceil(loader.bag_size.size()[0] * 0.8)
        self.test_offset = math.ceil(loader.bag_size.size()[0] * 0.9)
        self.loader = loader

        self.bag_sizes_train = loader.bag_size[0:self.validation_offset].squeeze()
        # self.imgs_train = loader.bags_of_images[0:sum(self.bag_sizes_train).long()]
        self.bags_creation_year_train = loader.bags_creation_year[0:sum(self.bag_sizes_train).long()]

        # validation data
        self.bag_sizes_validation = loader.bag_size[self.validation_offset:self.test_offset].squeeze()
        # self.imgs_validation = loader.bags_of_images[
        #                        sum(self.bag_sizes_train).long():sum(self.bag_sizes_train).long()
        #                                                        + sum(self.bag_sizes_validation.long())]
        self.bags_creation_year_validation = loader.bags_creation_year[
                                             sum(self.bag_sizes_train).long():sum(self.bag_sizes_train).long() + sum(
                                                 self.bag_sizes_validation.long())]

        # test data
        # self.imgs_test = loader.bags_of_images[500:sum(self.bag_sizes_train.long()[500:550])]
        self.bag_sizes_test = loader.bag_size[self.test_offset:len(loader.bag_size)].squeeze()
        # self.imgs_validation = loader.bags_of_images[
        #                        sum(self.bag_sizes_train).long():sum(self.bag_sizes_train).long()
        #                                                        + sum(self.bag_sizes_validation.long())]
        self.bags_creation_year_test = loader.bags_creation_year[
                                       sum(self.bag_sizes_train).long() + sum(self.bag_sizes_validation).long():len(
                                           loader.bags_creation_year)]

        self.device = loader.device

        # train features
        self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train \
            = self.init_priors(self.bag_sizes_train, self.bags_creation_year_train)

        # validation features
        self.birth_ranges_validation, self.feasible_birth_years_validation, self.birth_priors_validation \
            = self.init_priors(self.bag_sizes_validation, self.bags_creation_year_validation)

        # test features
        self.birth_ranges_test, self.feasible_birth_years_test, self.birth_priors_test \
            = self.init_priors(self.bag_sizes_test, self.bags_creation_year_test)

        self.e_layer = Expectation(self.device).to(self.device)

        self.bags_real_birth_year = loader.bags_real_birth_year
        self.bags_real_birth_year_train = loader.bags_real_birth_year[self.train_offset:self.validation_offset]
        self.bags_real_birth_year_validation = loader.bags_real_birth_year[self.validation_offset:self.test_offset]
        self.bags_real_birth_year_test = loader.bags_real_birth_year[self.test_offset:len(loader.bags_real_birth_year)]

    def e_single_bag(self, real_birth_years, data_offset, bag_i, model, bag_sizes, birth_priors, birth_ranges,
                     bags_creation_year,
                     feasible_birth_years):
        age_d = torch.Tensor([]).to(self.device)
        offset = sum(bag_sizes[0:bag_i].long())
        imgs = self.loader.load_bag(data_offset + bag_i).view(-1, 1, 64, 64)
        age_distr = model(imgs).view(bag_sizes.long()[bag_i], 100).to(self.device)
        del imgs
        birth_d = self.e_layer.forward(
            age_distr, bag_i, birth_priors, birth_ranges[bag_i],
            bags_creation_year[offset:offset + bag_sizes.long()[bag_i]], bag_sizes, birth_ranges,
            feasible_birth_years).to(self.device)

        age_d = torch.cat((age_d, age_distr))

        target = real_birth_years[bag_i]
        prediction = feasible_birth_years[torch.argmax(birth_d)]
        return age_d, birth_d, prediction, target

    def m_single_bag(self, data_offset, bag_i, age_d, birth_d, feasible_birth_years, bag_sizes, creation_years,
                     birth_ranges):
        offset = sum(bag_sizes[0:bag_i].long())
        smallest_year = feasible_birth_years[0]
        loss = torch.Tensor([0])
        bag_i_start = offset
        bag_i_end = (offset + bag_sizes[bag_i]).long()
        bag_size = bag_sizes[bag_i].long().item()

        bag_creations = creation_years[bag_i_start:bag_i_end]

        # distribution indices
        ages = torch.arange(0, 100).repeat(bag_size, 1).long().to(self.device)
        img_idx = torch.t(torch.arange(0, bag_size).repeat(ages.size()[1], 1)).long()
        bag_age_d = age_d[img_idx, ages]

        # betas
        diff_idx = (torch.t(bag_creations.repeat(ages.size()[1], 1)) - ages)
        alpha_idx_feasible = ((diff_idx >= birth_ranges[bag_i][0]) & (diff_idx <= birth_ranges[bag_i][1]))
        betas = torch.Tensor([smallest_year]).repeat(diff_idx.size()[0], diff_idx.size()[1]).to(self.device)
        betas[alpha_idx_feasible] = diff_idx[alpha_idx_feasible]
        betas = (betas - smallest_year)
        betas[betas != 0] = birth_d[betas[betas != 0].long()]
        loss = loss + torch.sum(betas * torch.log(bag_age_d))

        birth_priors = self.calculate_priors(birth_d.detach().cpu(), bag_sizes.detach().cpu(),
                                             feasible_birth_years.detach().cpu())

        return loss, birth_priors

    def em_single_bag(self, epochs):
        # model = Net().to(self.device)
        # model = Net().to(self.device).cuda(device='cuda:0')
        # optimizer = optim.Adam(model.parameters(), lr=0.01)
        best_validation_loss = float('inf')
        best_model = 0
        main_model = Net().to(self.device)
        best_validation_mae = float('inf')
        best_train_mae = float('inf')
        test_mae = float('inf')

        # train/validation
        for i in range(1, epochs + 1):
            # model.train()

            delta_params = {}
            delta_params_initialized = False
            general_loss = []

            birth_priors_train = torch.zeros(self.feasible_birth_years_train.size())

            prediction_train = []
            target_train = []

            prediction_validation = []
            target_validation = []
            for bag_i in range(0, len(self.bag_sizes_train.long())):
                model = Net().to(self.device)
                self.replace_params(main_model, model)

                model.train()
                optimizer = optim.Adam(model.parameters(), lr=0.01)
                initial_params = copy.deepcopy(main_model.state_dict())
                if not delta_params_initialized:
                    for k in initial_params:
                        delta_params[k] = torch.zeros(initial_params[k].size())
                    delta_params_initialized = True

                age_d, birth_d, prediction, target = self.e_single_bag(self.bags_real_birth_year_train,
                                                                       self.train_offset,
                                                                       bag_i,
                                                                       model,
                                                                       self.bag_sizes_train,
                                                                       self.birth_priors_train,
                                                                       self.birth_ranges_train,
                                                                       self.bags_creation_year_train,
                                                                       self.feasible_birth_years_train)
                prediction_train.append(prediction.item())
                target_train.append(target.item())

                loss, birth_priors = self.m_single_bag(self.train_offset,
                                                       bag_i,
                                                       age_d,
                                                       birth_d,
                                                       self.feasible_birth_years_train,
                                                       self.bag_sizes_train,
                                                       self.bags_creation_year_train,
                                                       self.birth_ranges_train)
                birth_priors_train = birth_priors_train + birth_priors
                loss = -loss
                if math.isinf(loss.item()):
                    print("inf for bag " + str(bag_i))
                    continue
                general_loss.append(loss.item())
                loss.backward()
                optimizer.step()
                new_params = copy.deepcopy(model.state_dict())
                # new - old

                for k, v in initial_params.items():
                    delta_params[k] = delta_params[k] + new_params[k] - initial_params[k]

            self.apply_delta_params(main_model, delta_params)
            # train_mae_mean = f.l1_loss(torch.Tensor(prediction_train).squeeze(),
            #                            torch.Tensor(target_train).squeeze()).float().item()

            self.birth_priors_train = birth_priors_train

            MAE_t = np.abs(np.array(target_train) - np.array(prediction_train))
            weighted_MAE_t = np.dot(MAE_t, self.bag_sizes_train)
            train_mae_mean = (weighted_MAE_t / sum(self.bag_sizes_train).item())

            if best_train_mae > train_mae_mean:
                best_train_mae = train_mae_mean

            # validation
            validation_loss = []
            with torch.no_grad():
                for bag_i in range(0, len(self.bag_sizes_validation.long())):
                    age_d, birth_d, prediction_v, target_v = self.e_single_bag(self.bags_real_birth_year_validation,
                                                       self.validation_offset,
                                                       bag_i,
                                                       main_model,
                                                       self.bag_sizes_validation,
                                                       self.birth_priors_validation,
                                                       self.birth_ranges_validation,
                                                       self.bags_creation_year_validation,
                                                       self.feasible_birth_years_validation)

                    prediction_validation.append(prediction_v)
                    target_validation.append(target_v)

                    loss, birth_priors = self.m_single_bag(self.validation_offset,
                                                           bag_i,
                                                           age_d,
                                                           birth_d,
                                                           self.feasible_birth_years_validation,
                                                           self.bag_sizes_validation,
                                                           self.bags_creation_year_validation,
                                                           self.birth_ranges_validation)
                    loss = -loss

                    validation_loss.append(loss.item())

                # print(
                #     "-------------------------------------------- TARGET VALIDATION -----------------------------------------------")
                # print(target_validation)
                # print(
                #     "-------------------------------------------- PREDICTION VALIDATION -----------------------------------------------")
                # print(prediction_validation)

            MAE_v = np.abs(np.array(target_validation) - np.array(prediction_validation))
            weighted_MAE_v = np.dot(MAE_v, self.bag_sizes_validation)
            validation_mae_mean = (weighted_MAE_v / sum(self.bag_sizes_validation).item())

            if best_validation_mae > validation_mae_mean:
                best_validation_mae = validation_mae_mean
            print(
                'Epoch ' + str(i) + ', Train loss: ' + str(np.mean(general_loss)) + ', Train mae:' + str(
                    train_mae_mean) + ', Best train mae:' + str(best_train_mae)
                + ', Validation loss: ' + str(np.mean(validation_loss)) + ', Validation mae: ' + str(
                    validation_mae_mean) + ', Best validation mae:' + str(best_validation_mae))
            # print('Epoch ' + str(i) + ', Train loss: ' + str(general_loss))

            if np.mean(best_validation_loss) > np.mean(validation_loss):
                torch.save(main_model, 'best_model.pt')
                best_validation_loss = validation_loss
        # test
        print("-------------------------------------------- TEST -----------------------------------------------")
        test_model = torch.load('best_model.pt')
        prediction_test = []
        target_test = []
        with torch.no_grad():
            test_loss = 0
            for bag_i in range(0, len(self.bag_sizes_test.long())):
                age_d, birth_d, prediction, target = self.e_single_bag(self.bags_real_birth_year_test,
                                                                       self.test_offset,
                                                                       bag_i,
                                                                       test_model,
                                                                       self.bag_sizes_test,
                                                                       self.birth_priors_test,
                                                                       self.birth_ranges_test,
                                                                       self.bags_creation_year_test,
                                                                       self.feasible_birth_years_test)

                prediction_test.append(prediction)
                target_test.append(target)

                loss, birth_priors = self.m_single_bag(self.test_offset,
                                                       bag_i,
                                                       age_d,
                                                       birth_d,
                                                       self.feasible_birth_years_test,
                                                       self.bag_sizes_test,
                                                       self.bags_creation_year_test,
                                                       self.birth_ranges_test)
                loss = -loss

                test_loss = test_loss + loss.item()
        print('Test loss: ' + str(test_loss))

    def apply_delta_params(self, model, new_params):
        params = model.state_dict()
        for name1, param1 in params.items():
            val = params[name1].data + new_params[name1]
            params[name1] = val
        model.load_state_dict(params)

    def replace_params(self, main_model, model):
        main_model_params = main_model.state_dict()
        model_params = model.state_dict()

        for name1, param1 in main_model_params.items():
            model_params[name1] = param1.data
        model.load_state_dict(model_params)

    def e(self, model, bag_sizes, birth_priors, birth_ranges, bags_creation_year, feasible_birth_years):
        birth_d = torch.Tensor([]).to(self.device)
        age_d = torch.Tensor([]).to(self.device)
        offset = 0
        for bag_i in range(0, len(bag_sizes.long())):
            # imgs = data[offset:offset + bag_sizes.int()[bag_i]].view(-1, 1, 64, 64)
            imgs = self.loader.load_bag(bag_i).view(-1, 1, 64, 64)
            print('img', torch.cuda.memory_allocated(device='cuda:0'))
            age_distr = model(imgs).view(bag_sizes.long()[bag_i], 100).to(self.device)
            print('model', torch.cuda.memory_allocated(device='cuda:0'))
            print(bag_i)
            print(bag_sizes[bag_i])
            del imgs
            birth_d = torch.cat((birth_d, self.e_layer.forward(
                age_distr, bag_i, birth_priors, birth_ranges[bag_i],
                bags_creation_year[offset:offset + bag_sizes.long()[bag_i]], bag_sizes, birth_ranges,
                feasible_birth_years).to(self.device)))

            age_d = torch.cat((age_d, age_distr))
            offset = offset + bag_sizes.long()[bag_i]
        return age_d, birth_d

    def m(self, age_d, birth_d, feasible_birth_years, bag_sizes, creation_years, birth_ranges, optimizer, train=True):
        offset = torch.Tensor([0])
        f_births_len = feasible_birth_years.size()[0]
        smallest_year = feasible_birth_years[0]
        loss = 0
        for bag_i in range(0, len(bag_sizes.int())):
            bag_i_start = offset.int()
            bag_i_end = (offset + bag_sizes[bag_i]).int()
            bag_size = bag_sizes[bag_i].int().item()
            bag_creations = creation_years[bag_i_start:bag_i_end]
            birth_d_bag = birth_d[bag_i * f_births_len:bag_i * f_births_len + f_births_len]

            # distribution indices
            ages = torch.arange(0, 100).repeat(bag_size, 1).long().to(self.device)
            img_idx = torch.t(torch.arange(0, bag_size).repeat(ages.size()[1], 1)).long()
            bag_age_d = age_d[img_idx, ages]

            # betas
            diff_idx = torch.t(bag_creations.repeat(ages.size()[1], 1)) - ages
            alpha_idx_feasible = ((diff_idx >= birth_ranges[bag_i][0]) & (diff_idx <= birth_ranges[bag_i][1]))
            betas = torch.Tensor([smallest_year]).repeat(diff_idx.size()[0], diff_idx.size()[1]).to(self.device)
            betas[alpha_idx_feasible] = diff_idx[alpha_idx_feasible]
            betas = (betas - smallest_year)
            betas[betas != 0] = birth_d_bag[betas[betas != 0].long()]
            loss = loss + torch.sum(betas * torch.log(bag_age_d))

            offset = offset + bag_sizes[bag_i]

        birth_priors = self.calculate_priors(birth_d, bag_sizes, feasible_birth_years)

        loss = -loss
        if train:
            loss.backward()
            optimizer.step()
        return loss.item(), birth_priors

    def refresh(self, optimizer, bag_sizes, data, bags_creation_year, birth_ranges, feasible_birth_years, birth_priors):
        optimizer.zero_grad()
        return optimizer, bag_sizes.clone().detach(), data.clone().detach(), bags_creation_year.clone().detach(), \
               birth_ranges.clone().detach(), feasible_birth_years.clone().detach(), birth_priors.clone().detach()

    def calculate_priors(self, birth_d, bag_sizes, feasible_birth_years):
        return torch.sum(
            bag_sizes.repeat(feasible_birth_years.size()[0], 1) * birth_d.view(feasible_birth_years.size()[0], -1), 1) / \
               torch.sum(
                   bag_sizes.repeat(feasible_birth_years.size()[0], 1) * birth_d.view(feasible_birth_years.size()[0],
                                                                                      -1))

    def em(self, epochs):
        model = Net().to(self.device).cuda(device='cuda:0')
        optimizer = optim.SGD(model.parameters(), lr=0.001)
        best_validation_loss = float('inf')
        best_model = 0

        # train/validation
        for i in range(1, epochs + 1):
            model.train()
            train_bag_limit = round(self.loader.bag_size.size()[0] * 0.7)
            birth_ranges_train, feasible_birth_years_train, birth_priors_train \
                = self.init_priors(self.bag_sizes_train, self.bags_creation_year_train)
            bag_sizes_train = self.loader.bag_size[0:train_bag_limit].squeeze()
            # imgs_train = self.loader.bags_of_images[0:sum(self.bag_sizes_train).int()]
            bags_creation_year_train = self.loader.bags_creation_year[0:sum(self.bag_sizes_train).int()]

            # optimizer, self.bag_sizes_train, self.imgs_train, self.bags_creation_year_train, \
            # self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train = \
            #     self.refresh(optimizer, self.bag_sizes_train, self.imgs_train, self.bags_creation_year_train,
            #                  self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train)

            age_d, birth_d = self.e(model,
                                    bag_sizes_train,
                                    birth_priors_train,
                                    birth_ranges_train,
                                    bags_creation_year_train,
                                    feasible_birth_years_train)

            loss, birth_priors = self.m(age_d, birth_d, feasible_birth_years_train, bag_sizes_train,
                                        bags_creation_year_train, birth_ranges_train, optimizer, train=True)
            # validation
            # for p in model.parameters():
            #     if p.grad is not None:
            #         del p.grad  # free some memory
            # torch.cuda.empty_cache()
            # validation_loss = self.predict(model, optimizer, test=False)
            # print('Epoch ' + str(i) + ', Train loss: ' + str(loss) + ', Validation loss: ' + str(validation_loss))
            print('Epoch ' + str(i) + ', Train loss: ' + str(loss))

            # if best_validation_loss > validation_loss:
            #     torch.save(model, 'best_model.pt')
            #     best_validation_loss = validation_loss

        return best_model, best_validation_loss

    def predict(self, model, optimizer, test=True):
        with torch.no_grad():
            age_d, birth_d = self.e(model,
                                    self.imgs_validation,
                                    self.bag_sizes_validation,
                                    self.birth_priors_validation,
                                    self.birth_ranges_validation,
                                    self.bags_creation_year_validation,
                                    self.feasible_birth_years_validation)

            loss, birth_priors = self.m(age_d, birth_d, self.feasible_birth_years_validation, self.bag_sizes_validation,
                                        self.bags_creation_year_validation, self.birth_ranges_validation, optimizer,
                                        train=False)

        return loss

    def init_priors(self, bag_sizes, bag_creation_years):
        # min and max for each bag
        birth_ranges = torch.empty(len(bag_sizes), 2).to(self.device)
        offset = 0
        b_s = bag_sizes.long()
        for bag_i in range(0, len(b_s)):
            min_cr = min(bag_creation_years[offset:offset + b_s[bag_i]]).long()
            max_cr = max(bag_creation_years[offset:offset + b_s[bag_i]]).long()
            period = 99 - (max_cr - min_cr)
            birth_ranges[bag_i] = torch.Tensor([min_cr - period, min_cr])
            offset = offset + b_s[bag_i]
        bounds = torch.Tensor([torch.min(torch.t(birth_ranges)[0]), torch.max(torch.t(birth_ranges)[1])]).to(
            self.device)
        feasible_byears = torch.arange(bounds[0].item(), bounds[1].item() + 1).to(self.device)
        birth_priors = torch.Tensor([100 / feasible_byears.size()[0] / 100]).repeat(feasible_byears.size()[0]).to(
            self.device)
        return birth_ranges, feasible_byears, birth_priors
