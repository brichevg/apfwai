import torch as torch


class Expectation(torch.nn.Module):

    def __init__(self, device):
        super(Expectation, self).__init__()
        self.device = device

    def forward(self, age_distr, bag_i, birth_priors, bag_birth_range, bag_creation_years, bag_sizes, birth_ranges, feasible_birth_years):
        # first component
        first = torch.log(birth_priors).to(self.device)

        # second component
        br = torch.t(feasible_birth_years.repeat(bag_creation_years.size()[0], 1))
        cr = bag_creation_years.repeat(feasible_birth_years.size()[0], 1)
        ages = cr - br
        feasible_ages = (ages >= 0) & (ages < 100)

        # which birth dates are eligible for this bag of photos (creation times - birth date should be [0,
        # 100] for each photo, cause if model(creat_x - birth|x) is 0, then  log(0) is -inf and the whole summation
        # will be - inf)
        fit_range = torch.prod(feasible_ages, dim=1).nonzero().view(-1)

        # repeating for the vectorization in formula
        # 1.repeating years for each photo
        years = feasible_birth_years[torch.prod(feasible_ages, dim=1).nonzero()].repeat(1, bag_creation_years.size()[0])

        # 2.repeating creation times for each image accross years
        creations = bag_creation_years.repeat(years.size()[0], 1)

        # age indices for each image
        age_i = torch.t(creations - years)
        img_i = torch.t(torch.arange(0, bag_sizes[bag_i].item()).repeat(fit_range.size()[0], 1)).to(self.device)
        second = torch.Tensor([float('-inf')]).repeat(feasible_birth_years.size()).to(self.device)
        second[fit_range] = torch.sum(torch.log(age_distr[img_i.long(), age_i.long()]), dim=0).to(self.device)

        # third
        bag_f_b = torch.arange(bag_birth_range[0].item(), bag_birth_range[1].item() + 1).to(self.device)
        age_i = torch.t(bag_creation_years.repeat(bag_f_b.size()[0], 1)) - bag_f_b.repeat(bag_creation_years.size()[0], 1)
        img_i = torch.t(torch.arange(0, bag_sizes[bag_i].item()).repeat(bag_f_b.size()[0], 1)).to(self.device)
        age_p_prod = torch.prod(age_distr[img_i.long(), age_i.long()].double(), dim=0)
        bag_birth_p = birth_priors[(feasible_birth_years >= bag_birth_range[0]) & (feasible_birth_years <= bag_birth_range[1])]
        third = torch.log(torch.sum(age_p_prod * bag_birth_p)).to(self.device)
        return torch.exp(first + second - third).to(self.device)
