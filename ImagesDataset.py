from torch.utils.data import Dataset

class ImagesDataset(Dataset):

    def __init__(self):
        self.samples = []

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]

    def push(self, element):
        self.samples.append(element)

