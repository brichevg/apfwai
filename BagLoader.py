import itertools
from csv import reader
import cv2 as cv
import os
import numpy as np
import random
import torch as torch
from itertools import islice


class BagLoader:
    def __init__(self, features_path, device=torch.device('cpu'), bag_limit=100):
        self.device = device
        self.bags_of_images = torch.empty(0).to(device=self.device)
        self.bags_creation_year = torch.empty(0).to(device=self.device)
        self.bags_real_birth_year = torch.empty(0).to(device=self.device)
        self.bag_size = torch.empty(0).to(device=self.device)
        self.features_path = features_path
        self.bag_limit = bag_limit

    def load(self):
        BagLoader.prepare_images('features/morph_bags1.csv')
        print('loading data...')
        bags_of_images_dict = {}
        bags_creation_year_dict = {}
        bags_birth_year_dict = {}
        bag_size = {}

        with open(self.features_path, 'r') as read_obj:
            data = reader(read_obj)
            for features in itertools.islice(data, 1, None):
                img = cv.imread(os.path.join('data/', features[0]))
                img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
                name = features[9]
                if name not in bags_of_images_dict:
                    bags_of_images_dict[name] = torch.Tensor([])
                    bag_size[name] = torch.Tensor([0])
                    bags_creation_year_dict[name] = torch.Tensor([])
                    bags_birth_year_dict[name] = BagLoader.generate_birth_date()

                by = bags_birth_year_dict[name]
                age = int(features[10])
                bag_size[name] = bag_size[name] + 1
                bags_of_images_dict[name] = torch.cat((bags_of_images_dict[name], torch.Tensor([img])), dim=0)
                bags_creation_year_dict[name] = torch.cat((bags_creation_year_dict[name], torch.Tensor([by + age])))

        sorted_bag_size = [bag_size[key] for key in sorted(bag_size.keys())[0:self.bag_limit]]
        sorted_bags_of_images_dict = [bags_of_images_dict[key] for key in
                                      sorted(bags_of_images_dict.keys())[0:self.bag_limit]]
        sorted_bags_creation_year_dict = [bags_creation_year_dict[key] for key in
                                          sorted(bags_creation_year_dict.keys())[0:self.bag_limit]]
        sorted_bags_birth_year_dict = [bags_birth_year_dict[key] for key in
                                          sorted(bags_birth_year_dict.keys())[0:self.bag_limit]]

        self.bag_size = torch.stack(sorted_bag_size).to(self.device)
        self.bags_of_images = torch.cat(sorted_bags_of_images_dict).to(self.device)
        self.bags_creation_year = torch.cat(sorted_bags_creation_year_dict).to(self.device)
        self.bags_real_birth_year = torch.Tensor(sorted_bags_birth_year_dict).to(self.device)

        # self.to_tensors(bags_of_images_dict, bags_creation_year_dict, bags_birth_year_dict)

        print('loading data completed..')

    @staticmethod
    def adjust_img(path, landmarks):
        img = cv.imread(os.path.join('data/', path))
        margin = (landmarks[1, 0] - landmarks[0, 0]) * 0.125

        new_landmarks = [landmarks[0, 0] - margin,
                         landmarks[1, 0] + margin,
                         landmarks[0, 1] - margin,
                         landmarks[2, 1] + margin]
        border_size = 0
        if min(new_landmarks) < 0:
            border_size = np.round(abs(min(new_landmarks))).astype(int)
            new_landmarks = [new_landmarks[0] + border_size,
                             new_landmarks[1] - border_size,
                             new_landmarks[2] + border_size,
                             new_landmarks[3] - border_size]
        new_landmarks = np.array(new_landmarks).astype(int)
        crop_img = img[new_landmarks[0]:new_landmarks[1], new_landmarks[2]:new_landmarks[3]]
        with_border = cv.copyMakeBorder(crop_img, border_size, border_size, border_size, border_size,
                                        cv.BORDER_CONSTANT)
        small_img = cv.resize(with_border, (64, 64))

        # greyscale
        gray = cv.cvtColor(small_img, cv.COLOR_BGR2GRAY)

        return gray

    def take(self, n, iterable):
        "Return first n items of the iterable as a list"
        return list(islice(iterable, n))

    @staticmethod
    def get_landmarks(features):
        landmarks = features[1:9]
        landmarks = np.asarray(landmarks)
        landmarks = landmarks.astype('int').reshape(-1, 2)
        return landmarks

    @staticmethod
    def generate_birth_date():
        start_year = 1900
        end_year = 2020
        random_range = random.randrange(end_year - start_year)
        return start_year + random_range

    @staticmethod
    def prepare_images(source_feature_path, abs_destination_img_path = 'data/AgeGenderFaces/adjusted_data'):
        try:
            os.mkdir(abs_destination_img_path)
            print("Directory ", abs_destination_img_path, " Created ")
            print('preparing data...')
            with open(source_feature_path, 'r') as read_obj:
                data = reader(read_obj)
                for features in itertools.islice(data, 1, None):
                    img_name = features[0].split('/')[-1]
                    img = BagLoader.adjust_img(features[0], BagLoader.get_landmarks(features))
                    cv.imwrite(os.path.join(abs_destination_img_path, img_name), img)
            print('finished')
        except FileExistsError:
            print("Directory ", abs_destination_img_path, " already exists")


