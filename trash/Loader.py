import itertools
from csv import reader
import cv2 as cv
import os
import numpy as np
import datetime
import random

from ImagesDataset import ImagesDataset


class Loader:

    def __init__(self):

        self.training = ImagesDataset()
        self.validation = ImagesDataset()
        self.test = ImagesDataset()
        self.bags = {}
        self.bags_creation_time = {}
        self.bags_birth_date = {}
        self.features_path = 'features/morph_bags_adjusted_grouped.csv'
        self.CPU_SAMPLE_SIZE = 300

    def load(self):
        with open(self.features_path, 'r') as read_obj:
            data = reader(read_obj)

            for features in itertools.islice(data, 1, None):
                img = cv.imread(os.path.join('data/', features[0]))

                if img is None:
                    continue
                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

                folder = int(features[13])

                if folder in range(4, 11):
                    self.training.push((gray, float(features[10])))

                elif folder in range(3, 4):
                    self.validation.push((gray, float(features[10])))

                else:
                    self.test.push((gray, float(features[10])))

    def load_for_cpu(self):
        with open(self.features_path, 'r') as read_obj:
            data = reader(read_obj)

            train_count = 0
            valid_count = 0
            test_count = 0

            for features in itertools.islice(data, 1, None):

                img = cv.imread(os.path.join('data/', features[0]))

                if img is None:
                    continue

                gray = gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

                if gray is None:
                    continue

                folder = int(features[13])

                if folder in range(4, 11):
                    if train_count == self.CPU_SAMPLE_SIZE:
                        continue
                    self.training.push((gray, float(features[10])))
                    train_count += 1

                elif folder in range(3, 4):
                    if valid_count == self.CPU_SAMPLE_SIZE:
                        continue
                    self.validation.push((gray, float(features[10])))
                    valid_count += 1
                else:
                    if test_count == self.CPU_SAMPLE_SIZE / 2:
                        continue
                    self.test.push((gray, float(features[10])))
                    test_count += 1

                if train_count == self.CPU_SAMPLE_SIZE & valid_count == self.CPU_SAMPLE_SIZE & test_count == self.CPU_SAMPLE_SIZE / 2:
                    return

    def load_bags(self):
        with open(self.features_path, 'r') as read_obj:
            data = reader(read_obj)

            for features in itertools.islice(data, 1, None):
                img = self.adjust_img(features[0], self.get_landmarks(features))
                name = features[9]
                if name not in self.bags:
                    self.bags[name] = {}
                    self.bags_creation_time[name] = []
                    self.bags_birth_date[name] = self.genearte_birth_date()
                bd = self.bags_birth_date[name]
                age = int(features[10])
                self.bags_creation_time[name].append(bd + datetime.timedelta(days=age * 365))
                self.bags[name].append(img)

    def get_landmarks(self, features):
        landmarks = features[1:9]
        landmarks = np.asarray(landmarks)
        landmarks = landmarks.astype('int').reshape(-1, 2)
        return landmarks

    def adjust_img(self, path, landmarks):
        img = cv.imread(path)
        # plt.imshow(img)
        # plt.show()

        # landmarks + margin
        margin = (landmarks[1, 0] - landmarks[0, 0]) * 0.125

        new_landmarks = [landmarks[0, 0] - margin,
                         landmarks[1, 0] + margin,
                         landmarks[0, 1] - margin,
                         landmarks[2, 1] + margin]
        border_size = 0
        if min(new_landmarks) < 0:
            border_size = np.round(abs(min(new_landmarks))).astype(int)
            new_landmarks = [new_landmarks[0] + border_size,
                             new_landmarks[1] - border_size,
                             new_landmarks[2] + border_size,
                             new_landmarks[3] - border_size]
        new_landmarks = np.array(new_landmarks).astype(int)
        crop_img = img[new_landmarks[0]:new_landmarks[1], new_landmarks[2]:new_landmarks[3]]
        with_border = cv.copyMakeBorder(crop_img, border_size, border_size, border_size, border_size,
                                        cv.BORDER_CONSTANT)
        small_img = cv.resize(with_border, (64, 64))
        # plt.imshow(small_img)
        # plt.show()

        # greyscale
        gray = cv.cvtColor(small_img, cv.COLOR_BGR2GRAY)

        return gray

    def genearte_birth_date(self):
        start_date = datetime.date(1900, 1, 1)
        end_date = datetime.date(2020, 1, 1)
        time_between_dates = end_date - start_date
        days_between_dates = time_between_dates.days
        random_number_of_days = random.randrange(days_between_dates)
        return start_date + datetime.timedelta(days=random_number_of_days)
