import torch as torch
import numpy as np
import torch.nn as nn
from torch import optim
from torch.utils.data import DataLoader, SubsetRandomSampler
import torchvision.datasets as datasets
import torchvision
import torch.nn.functional as F
from Net import Net


def train_mnist():
    model = Net(10)
    loss_function = nn.NLLLoss()
    # loss_function = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    batch_size_train = 100
    train_indices = list(range(2000))
    train_sampler = SubsetRandomSampler(train_indices)
    for epoch in range(1, 1000):
        train_loss, valid_loss, valid_acc = [], [], []
        model.train()

        # create batches
        dataloader_train = DataLoader(
            datasets.MNIST(root='./data/mnist', train=False, download=True,
                           transform=torchvision.transforms.ToTensor()), batch_size=batch_size_train, shuffle=True)
        # dataloader_valid = DataLoader(loader.validation, batch_size=10, shuffle=True)

        # matrix of age diffs
        # age_diffs = abs(torch.arange(10).reshape(-1, 1) - torch.arange(10)).repeat(batch_size_train, 1)
        for data, target in dataloader_train:
            data = F.upsample(data, size=(64, 64), mode='bilinear')
            data = data.view(-1, 1, 64, 64).float().clone().detach().requires_grad_(True)
            distribution = model(data).view(batch_size_train, 10)

            # prediction
            batch_prediction = torch.argmax(distribution, dim=1)

            # argmin_by_y (sum_by_y'[p(y'|x)*|y' - y|]
            # batch_prediction = (distribution.view(batch_size_train, 10).repeat(1, 10)
            #                     .view(batch_size_train * 10, 10) * age_diffs).sum(dim=1)\
            #     .view(batch_size_train, 10).argmin(dim=1)

            # loss
            optimizer.zero_grad()
            loss = loss_function(distribution.log(), target)
            # loss = torch.mean((batch_prediction != target).float().requires_grad_(True))
            # loss = loss_function(batch_prediction.float(), target.float().requires_grad_(True))
            train_loss.append(loss.item())
            print("Train acc: ", torch.mean((batch_prediction == target).float()).item(), loss.item())
            loss.backward()
            optimizer.step()

        # evaluation part
        model.eval()

        for batch in dataloader_valid:
            target = torch.tensor(batch[0], dtype=torch.float, requires_grad=True)
            data = batch[1]
            distribution = model(data.view(-1, 1, 64, 64).float())

            # prediction:
            # argmin_by_y (sum_by_y'[p(y'|x)*|y' - y|]
            batch_prediction = torch.sum((distribution.view(10, 100).repeat(1, 100).view(1000, 100) * age_diffs),
                                         dim=1).view(10, 100).argmin(dim=1)
            loss = loss_function(batch_prediction, target)
            valid_loss.append(loss.item())
            acc = torch.mean((torch.tensor(batch_prediction) == target).float())
            valid_acc.append(acc.item())

        print("Epoch:", epoch, "Training Loss: ", np.mean(train_loss), "Valid Loss: ", np.mean(valid_loss),
              "Valid Accuracy: ", np.mean(valid_acc))

    # compound label gender x age
    # margin - 0.25
    # mse