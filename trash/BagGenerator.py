import datetime
import itertools
import random
from csv import reader
import cv2 as cv
import os
import shutil

from trash.Loader import Loader

BAG_SIZE = 20

loader = Loader()

try:
    shutil.rmtree('data/bags')
except:
    print("Error while deleting file ", 'data/bags')
os.mkdir('data/bags')


# birth date is the directory name, creation date is the image name
def genearte_birth_date():
    start_date = datetime.date(1900, 1, 1)
    end_date = datetime.date(2020, 1, 1)
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    return start_date + datetime.timedelta(days=random_number_of_days)


with open('data/morph_bags1.csv', 'r') as read_obj:
    data = reader(read_obj)
    count_img = 0
    count_bags = 0
    folder_path = ''
    for features in itertools.islice(data, 1, None):
        if count_img == BAG_SIZE:
            count_img = 0
        if count_img == 0:
            count_bags += 1
            birth_date = genearte_birth_date()
            folder_path = 'data/bags/' + str(count_bags) + '_' + str(birth_date)
            os.mkdir(folder_path)
        landmarks = loader.get_landmarks(features)
        img = loader.adjust_img(features[0], landmarks)
        if img is None:
            continue
        count_img += 1
        cv.imwrite(folder_path + '/' + str(count_img) + '_' + str(
            birth_date + datetime.timedelta(days=float(features[10]) * 365)) + '.jpg', img)
