from BagLoader import BagLoader
import torch
import NnGenerator as nng

from EM import EM


def get_device(cuda_device='cuda:0'):
    if torch.cuda.is_available():
        device = torch.device(cuda_device)
        print('Using GPU')
    else:
        device = torch.device('cpu')
        print('Using CPU')
    return device


def do_EM():
    loader = BagLoader('features/morph_bags_adjusted.csv', device=get_device(), bag_limit=100)
    loader.load()

    # 30 epochs
    EM(loader).em(3000)
    #EM(loader).evaluate_e()
#do_EM()


nng.train_agedb(only_test=False)



