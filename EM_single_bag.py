import copy
import math
import os
import shutil

import numpy as np
from torch import optim, nn
from torch.nn import functional as f
from Expectation import Expectation
from Net import Net
import matplotlib
import tkinter

matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import torch as torch


class EM_single_bag:
    def __init__(self, loader):
        # training data
        self.train_offset = 0
        self.validation_offset = math.ceil(loader.bag_size.size()[0] * 0.8)
        self.test_offset = math.ceil(loader.bag_size.size()[0] * 0.9)
        self.loader = loader

        self.bag_sizes_train = loader.bag_size[0:self.validation_offset].squeeze()
        self.bags_creation_year_train = loader.bags_creation_year[0:sum(self.bag_sizes_train).long()]

        # validation data
        self.bag_sizes_validation = loader.bag_size[self.validation_offset:self.test_offset].squeeze()
        self.bags_creation_year_validation = loader.bags_creation_year[
                                             sum(self.bag_sizes_train).long():sum(self.bag_sizes_train).long() + sum(
                                                 self.bag_sizes_validation.long())]

        # test data
        self.bag_sizes_test = loader.bag_size[self.test_offset:len(loader.bag_size)].squeeze()
        self.bags_creation_year_test = loader.bags_creation_year[
                                       sum(self.bag_sizes_train).long() + sum(self.bag_sizes_validation).long():len(
                                           loader.bags_creation_year)]

        self.device = loader.device

        self.birth_ranges_train, self.feasible_birth_years_train, self.birth_priors_train \
            = self.init_all_priors(loader.bag_size, loader.bags_creation_year)

        # # validation features
        # self.birth_ranges_validation, self.feasible_birth_years_validation, self.birth_priors_validation \
        #     = self.init_all_priors(self.bag_sizes_validation, self.bags_creation_year_validation)
        #
        # # test features
        # self.birth_ranges_test, self.feasible_birth_years_test, self.birth_priors_test \
        #     = self.init_priors(self.bag_sizes_test, self.bags_creation_year_test)

        self.e_layer = Expectation(self.device).to(self.device)

        self.bags_real_birth_year_train = loader.bags_real_birth_year[self.train_offset:self.validation_offset]
        self.bags_real_birth_year_validation = loader.bags_real_birth_year[self.validation_offset:self.test_offset]
        self.bags_real_birth_year_test = loader.bags_real_birth_year[self.test_offset:len(loader.bags_real_birth_year)]

    def plot_alphas(self, bag_sizes, alphas, epoch, loss):
        f = plt.figure(0)
        alphas_formatted = alphas.view(bag_sizes.size()[0], self.feasible_birth_years_train.size()[0])
        bag_i = 0
        for bag_i_x in range(0, 5):
            for bag_i_y in range(0, 4):
                if bag_i == len(bag_sizes):
                    break
                plt.subplot2grid((5, 4), (bag_i_x, bag_i_y))
                # plt.subplot2grid((math.ceil(len(bag_sizes) / 2), math.floor(len(bag_sizes) / 2)), (bag_i_x, bag_i_y))
                plt.plot(self.feasible_birth_years_train.cpu().clone().numpy(),
                         alphas_formatted[bag_i].detach().cpu().clone().numpy())
                plt.axvline(x=self.bags_real_birth_year_train[bag_i], color='red')
                bag_i = bag_i + 1
        if loss != -1:
            f.suptitle('Epoch #{}, Loss: {}'.format(epoch, loss), fontsize=20)
        # plt.show()
        plt.draw()
        f.savefig('posterior/img_{}.png'.format(epoch))
        plt.clf()

    def init_all_priors(self, bag_sizes, bag_creation_years):
        # min and max for each bag
        birth_ranges = torch.empty(bag_sizes.size()[0], 2)
        offset = 0
        b_s = bag_sizes.int()
        for bag_i in range(0, len(b_s)):
            min_cr = min(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            max_cr = max(bag_creation_years[offset:offset + b_s[bag_i]]).int()
            period = 99 - (max_cr - min_cr)
            birth_ranges[bag_i] = torch.Tensor([min_cr - period, min_cr])
            offset = offset + b_s[bag_i]
        bounds = torch.Tensor([torch.min(torch.t(birth_ranges)[0]), torch.max(torch.t(birth_ranges)[1])])
        feasible_byears = torch.arange(bounds[0].item(), bounds[1].item() + 1)
        birth_priors = torch.Tensor([100 / feasible_byears.size()[0] / 100]).repeat(feasible_byears.size()[0])
        return birth_ranges, feasible_byears, birth_priors

    def calculate_priors(self, birth_d, bag_sizes):

        bag_sizes_repeated = bag_sizes.repeat(self.feasible_birth_years_train.size()[0], 1)
        birth_d_repeated = torch.t(birth_d.view(bag_sizes.size()[0], self.feasible_birth_years_train.size()[0]))
        return torch.sum(bag_sizes_repeated * birth_d_repeated, 1) / sum(
            torch.sum(bag_sizes_repeated * birth_d_repeated, 1))

    def e_single_bag(self, real_birth_years, data_offset, bag_i, model, bag_sizes, birth_priors, birth_ranges,
                     bags_creation_year,
                     feasible_birth_years):
        mae = nn.L1Loss()
        age_d = torch.Tensor([]).to(self.device)
        offset = sum(bag_sizes[0:bag_i].long())
        imgs = self.loader.load_bag(data_offset + bag_i).view(-1, 1, 64, 64).to(self.device)
        age_distr = model(imgs).view(bag_sizes.long()[bag_i], 100)
        del imgs
        birth_d = self.e_layer.forward(
            age_distr, bag_i, birth_priors, birth_ranges[bag_i],
            bags_creation_year[offset:offset + bag_sizes.long()[bag_i]], bag_sizes, birth_ranges,
            feasible_birth_years)

        age_d = torch.cat((age_d, age_distr))

        target = real_birth_years[bag_i]
        prediction_bag = self.prediction_for_bag(birth_d)
        mae_bag = mae(self.prediction_for_bag(birth_d), self.bags_real_birth_year_train[bag_i]).item()
        return age_d.to(self.device), birth_d.to(self.device), mae_bag, prediction_bag, target

    def m_single_bag(self, real_birth_dates, bag_i, age_d, birth_d, feasible_birth_years, bag_sizes, creation_years,
                     birth_ranges):
        offset = sum(bag_sizes[0:bag_i].long())
        smallest_year = feasible_birth_years[0]
        loss = torch.Tensor([0]).to(self.device)
        bag_i_start = offset
        bag_i_end = (offset + bag_sizes[bag_i]).long()
        bag_size = bag_sizes[bag_i].long().item()

        bag_creations = creation_years[bag_i_start:bag_i_end].to(self.device)

        # distribution indices
        ages = torch.arange(0, 100).repeat(bag_size, 1).long().to(self.device)
        img_idx = torch.t(torch.arange(0, bag_size).repeat(ages.size()[1], 1)).long()
        bag_age_d = age_d[img_idx, ages].to(self.device)

        # betas
        if real_birth_dates == None:
            diff_idx = (torch.t(bag_creations.repeat(ages.size()[1], 1)) - ages).to(self.device)
            alpha_idx_feasible = ((diff_idx >= birth_ranges[bag_i][0]) & (diff_idx <= birth_ranges[bag_i][1]))
            betas = torch.Tensor([smallest_year]).repeat(diff_idx.size()[0], diff_idx.size()[1]).to(self.device)
            betas[alpha_idx_feasible] = diff_idx[alpha_idx_feasible]
            betas = (betas - smallest_year)
            betas[betas != 0] = birth_d[betas[betas != 0].long()]
        else:
            true_labels = bag_creations - real_birth_dates[bag_i]
            betas = (ages==torch.t((bag_creations - real_birth_dates[bag_i]).repeat(100,1))).double()

        loss = loss + torch.mean(torch.sum((betas * torch.log(bag_age_d)), 1))

        return loss.to(self.device), birth_d

    def em_single_bag(self, epochs):
        best_validation_loss = float('inf')
        main_model = Net().to(self.device)
        best_validation_mae = float('inf')
        test_mae = float('inf')
        best_train_mae = float('inf')
        print(len(self.bag_sizes_train))
        try:
            shutil.rmtree('posterior')
            os.mkdir('posterior')
        except FileNotFoundError:
            os.mkdir('posterior')

        # train/validation
        for i in range(1, epochs + 1):
            # model.train()

            delta_params = {}
            delta_params_initialized = False
            general_loss = []

            birth_priors_train = torch.zeros(self.feasible_birth_years_train.size())

            mae_train = []
            prediction_train = []
            target_train = []

            prediction_validation = []
            target_validation = []
            birth_d_all = torch.Tensor().to(self.device)
            for bag_i in range(0, len(self.bag_sizes_train.long())):
                model = Net().to(self.device)
                self.replace_params(main_model, model)

                model.train()
                optimizer = optim.Adam(model.parameters(), lr=0.01)
                initial_params = copy.deepcopy(main_model.state_dict())
                if not delta_params_initialized:
                    for k in initial_params:
                        delta_params[k] = torch.zeros(initial_params[k].size()).to(self.device)
                    delta_params_initialized = True

                age_d, birth_d, mae_bag, prediction_bag, target = self.e_single_bag(self.bags_real_birth_year_train,
                                                                                    self.train_offset,
                                                                                    bag_i,
                                                                                    model,
                                                                                    self.bag_sizes_train,
                                                                                    self.birth_priors_train,
                                                                                    self.birth_ranges_train,
                                                                                    self.bags_creation_year_train,
                                                                                    self.feasible_birth_years_train)

                mae_train.append(mae_bag)
                prediction_train.append(prediction_bag)
                target_train.append(target.item())

                # if want to evaluate the m-step, set the real births
                loss, birth_d = self.m_single_bag(self.bags_real_birth_year_train,
                                                  bag_i,
                                                  age_d,
                                                  birth_d,
                                                  self.feasible_birth_years_train,
                                                  self.bag_sizes_train,
                                                  self.bags_creation_year_train,
                                                  self.birth_ranges_train)
                # birth_priors = self.calculate_priors(birth_d.detach().cpu(), bag_sizes.detach().cpu())
                #
                # birth_priors_train = birth_priors_train + birth_priors
                birth_d_all = torch.cat((birth_d_all, birth_d))
                loss = -loss
                general_loss.append(loss.item())
                loss.backward()
                optimizer.step()
                new_params = copy.deepcopy(model.state_dict())
                # new - old

                for k, v in initial_params.items():
                    delta_params[k] = delta_params[k] + new_params[k] - initial_params[k]

            self.birth_priors_train = self.calculate_priors(birth_d_all.detach().cpu(),
                                                            self.bag_sizes_train.detach().cpu())

            if i == 1 and not torch.cuda.is_available():
                self.plot_alphas(self.bag_sizes_train, birth_d_all, i, -1)
            if not torch.cuda.is_available() and i != 1:
                self.plot_alphas(self.bag_sizes_train, birth_d_all, i,
                                 str(self.weighted_loss(general_loss, self.bag_sizes_train)))

            self.apply_delta_params(main_model, delta_params)

            # print(
            #     "TARGET TRAIN: ",target_train)
            # # print(target_train)
            # print(
            #     "PREDICTION TRAIN: ",prediction_train)
            # print(prediction_train)

            train_mae_mean = (torch.mean(torch.Tensor(mae_train)).float()).item()

            if best_train_mae > train_mae_mean:
                best_train_mae = train_mae_mean

            print('Epoch ' + str(i) + ', Train loss: ' + str(self.weighted_loss(general_loss, self.bag_sizes_train))
                  + ', Train mae: ' + str(train_mae_mean) + ', Best train mae: ' + str(best_train_mae))

        torch.save(main_model, 'best_model.pt')

        #
        #     # validation
        #     validation_loss = []
        #     with torch.no_grad():
        #         for bag_i in range(0, len(self.bag_sizes_validation.long())):
        #             age_d, birth_d, prediction_v, target_v = self.e_single_bag(self.bags_real_birth_year_validation,
        #                                                                        self.validation_offset,
        #                                                                        bag_i,
        #                                                                        main_model,
        #                                                                        self.bag_sizes_validation,
        #                                                                        self.birth_priors_validation,
        #                                                                        self.birth_ranges_validation,
        #                                                                        self.bags_creation_year_validation,
        #                                                                        self.feasible_birth_years_validation)
        #
        #             prediction_validation.append(prediction_v)
        #             target_validation.append(target_v)
        #
        #             loss, birth_priors = self.m_single_bag(self.validation_offset,
        #                                                    bag_i,
        #                                                    age_d,
        #                                                    birth_d,
        #                                                    self.feasible_birth_years_validation,
        #                                                    self.bag_sizes_validation,
        #                                                    self.bags_creation_year_validation,
        #                                                    self.birth_ranges_validation)
        #             loss = -loss
        #
        #             validation_loss.append(loss.item())
        #
        #         # print(
        #         #     "-------------------------------------------- TARGET VALIDATION -----------------------------------------------")
        #         # print(target_validation)
        #         # print(
        #         #     "-------------------------------------------- PREDICTION VALIDATION -----------------------------------------------")
        #         # print(prediction_validation)
        #
        #     MAE_v = np.abs(np.array(target_validation) - np.array(prediction_validation))
        #     weighted_MAE_v = np.dot(MAE_v, self.bag_sizes_validation)
        #     validation_mae_mean = (weighted_MAE_v / sum(self.bag_sizes_validation).item())
        #
        #     if best_validation_mae > validation_mae_mean:
        #         best_validation_mae = validation_mae_mean
        #     print(
        #         'Epoch ' + str(i) + ', Train loss: ' + str(np.mean(general_loss)) + ', Train mae:' + str(
        #             train_mae_mean) + ', Best train mae:' + str(best_train_mae)
        #         + ', Validation loss: ' + str(np.mean(validation_loss)) + ', Validation mae: ' + str(
        #             validation_mae_mean) + ', Best validation mae:' + str(best_validation_mae))
        #     # print('Epoch ' + str(i) + ', Train loss: ' + str(general_loss))
        #
        #     if np.mean(best_validation_loss) > np.mean(validation_loss):
        #         torch.save(main_model, 'best_model.pt')
        #         best_validation_loss = validation_loss
        # # test
        # print("-------------------------------------------- TEST -----------------------------------------------")
        # test_model = torch.load('best_model.pt').cuda(device='cuda:0')
        # with torch.no_grad():
        #     test_loss = 0
        #     for bag_i in range(0, len(self.bag_sizes_test.long())):
        #         age_d, birth_d = self.e_single_bag(self.bags_real_birth_year_test,
        #                                            self.test_offset,
        #                                            bag_i,
        #                                            test_model,
        #                                            self.bag_sizes_test,
        #                                            self.birth_priors_test,
        #                                            self.birth_ranges_test,
        #                                            self.bags_creation_year_test,
        #                                            self.feasible_birth_years_test)
        #
        #         loss, birth_priors = self.m_single_bag(self.test_offset,
        #                                                bag_i,
        #                                                age_d,
        #                                                birth_d,
        #                                                self.feasible_birth_years_test,
        #                                                self.bag_sizes_test,
        #                                                self.bags_creation_year_test,
        #                                                self.birth_ranges_test)
        #         loss = -loss
        #
        #         test_loss = test_loss + loss.item()
        # print('Test loss: ' + str(test_loss))

    def weighted_loss(self, loss_v, bag_sizes):
        return torch.dot(torch.Tensor(loss_v), bag_sizes) / torch.sum(bag_sizes)

    def evaluate_m_step(self):
        pass

    def evaluate_e(self):
        try:
            os.mkdir('plots')
        except FileExistsError:
            shutil.rmtree('plots')
            os.mkdir('plots')
        model = torch.load('initial_model_learned_on_criminals.pt', map_location=torch.device('cpu'))
        model.eval()
        for bag_i in range(0, len(self.bag_sizes_train)):
            age_d, birth_d, mae_bag, prediction_bag, target = self.e_single_bag(self.bags_real_birth_year_train,
                                                                                self.train_offset,
                                                                                bag_i,
                                                                                model,
                                                                                self.bag_sizes_train,
                                                                                self.birth_priors_train,
                                                                                self.birth_ranges_train,
                                                                                self.bags_creation_year_train,
                                                                                self.feasible_birth_years_train)
            plt.axvline(x=self.bags_real_birth_year_train[bag_i], color='red')
            plt.plot(self.feasible_birth_years_train.cpu().clone().numpy(),
                     birth_d.detach().cpu().clone().numpy())
            # plt.show()
            plt.savefig('plots/bag_' + str(bag_i))
            plt.clf()

    def prediction_for_bag(self, alphas_for_bag):
        alphas_repeated = torch.t(alphas_for_bag.repeat(self.feasible_birth_years_train.size()[0], 1))
        fb_for_min_repeated = self.feasible_birth_years_train.repeat(alphas_for_bag.size()[0], 1)
        fb_for_sum_repeated = torch.t(self.feasible_birth_years_train.repeat(alphas_for_bag.size()[0], 1))
        sec = torch.abs(fb_for_min_repeated - fb_for_sum_repeated).to(self.device)
        sums = torch.sum(alphas_repeated * sec, 0)
        return self.feasible_birth_years_train[torch.argmin(sums)]

    def apply_delta_params(self, model, new_params):
        params = model.state_dict()
        for name1, param1 in params.items():
            val = params[name1].data + new_params[name1]
            params[name1] = val
        model.load_state_dict(params)

    def replace_params(self, main_model, model):
        main_model_params = main_model.state_dict()
        model_params = model.state_dict()

        for name1, param1 in main_model_params.items():
            model_params[name1] = param1.data
        model.load_state_dict(model_params)
